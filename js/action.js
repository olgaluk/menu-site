function getData() {   
    return new Promise((resolve)=>{
        var xhr = new XMLHttpRequest();
        xhr.open('Get', '../data.json', true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                  resolve(JSON.parse(xhr.responseText)); 
                }
            }
        }
        xhr.send();
    });
   
}
const DATA = getData();

//data.then(a => console.log(a));

/*function temporary() {
    return new Promise((resolve) => {
        setTimeout(()=>{
            resolve(5);
          }, 3000);
    });
    
}

var result = temporary();
result.then(function(param) {
    console.log(param);
})
*/

const CATEGORIES = 
[
    {
        id: 0,
        name: 'Первые блюда'
    },
    {
        id: 1,
        name: 'Вторые блюда'
    },
    {
        id: 2,
        name: 'Десерты'
    },
    {
        id: 3,
        name: 'Напитки'
    }
];

function generateFilterButton() {
 var filter = get('filter');
 CATEGORIES.forEach(category => {
     var button = document.createElement('div');
     button.className = 'filter__button';
     button.innerText = category.name;
     filter.appendChild(button);
 });
}

var get = function(name) {
    return document.getElementsByClassName(name)[0];
}

var selectorCategoryId = CATEGORIES[0].id;

window.onload = function() {
    generateFilterButton();

    DATA.then(result => {
     var dishes = result.filter(item => item.category_id == selectorCategoryId);
     console.log(dishes);
    });
}